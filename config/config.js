// config.js
const dotenv = require('dotenv');
dotenv.config();

const env = process.env.NODE_ENV; // 'dev' or 'prod'

const dev = {
    app: {
        port: 4501,
        server:'127.0.0.1'
    },
    db: {
        mongo_uri: process.env.MONGO_URI,
        db_username: process.env.DB_USERNAME,
        db_password: process.env.DB_PASSWORD
    },
   
};

const prod = {

    app: {
        port: 4501,
        server:'52.54.1.108'

    },
    db: {
        mongo_uri: process.env.MONGO_URI,
        db_username: process.env.DB_USERNAME,
        db_password: process.env.DB_PASSWORD
    },
}

const config = {
    dev,
    prod
};

module.exports = config[env];
