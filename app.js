const express = require('express')
const app = express()
var path = require('path');
require('dotenv').config()
const formidableMiddleware = require('express-formidable');

//local files
var apiRoutes = require('./routes/api_routes.js');
const config = require("./config/config");

//parse request get/post data
app.use(formidableMiddleware());

// configure stuff here
//app.locals.BASE_URL = `http://${process.env.SERVER_HOSTNAME}:${process.env.SERVER_PORT}/` 
//for making directory to be accessed from server
//app.use('/public',express.static(path.join(__dirname, '/public')));
//set static directories
//app.use(express.static(path.join(__dirname, 'views')));
//configure Routes
app.use('/api', apiRoutes)


//Middleware of application
function appMiddleware(req, res, next) {
  console.log("Request ---> ",new Date(), req.url)
  next()
}
app.use(appMiddleware)


app.get('/',function (req, res, next) {
    /**  res.sendFile(__dirname + "/index.html"); */
    res.writeHead(200, {'Content-Type': 'text/plain'});
    res.write('Streaming App is Working...');
    res.end();
    next() 
})

module.exports = app





/** 
app.get('*',function (req, res, next) {
    //to handle any unexpected url route
    res.redirect('/');
})
*/

//is same as 
//both above and below Cannot GET page.html error 


// app.use(function(req, res, next){
//     res.status(404);
//    // res.render(__dirname + '/index',{layout:false});
// })






