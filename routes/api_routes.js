const express = require('express')
const router = express.Router()
const apiControllerUser = require('../controllers/api_controller');

//routes
router.post('/stream' , apiControllerUser.streamPost) 
router.get('/video' , apiControllerUser.getVideo) 

module.exports = router
