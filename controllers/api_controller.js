var path = require('path');
const fs = require('fs');
const got = require('got');
const request = require('request');



exports.streamPost = async (req, res, next) => {
    console.log(req.fields.url)
    return res.status(400).json({
        "status":true,
        "message": "Sucessfull" 
    })

   // res.status(400).send("Requires Range header");
};



exports.getVideo = async (req, res, next) =>{
  var response = {}

  if(!req.query.url) {
    response['status'] = false
    response['message'] = "Provide URL"
    return res.status(400).json(response)
  }
    try{
           //var fileUrl = 'http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerEscapes.mp4';
            var fileUrl = req.query.url
            console.log(fileUrl)
            //var range = req.headers.range;
            var range = 'bytes=0-'
            var positions, start, end, total, chunksize;
            // HEAD request for file metadata
            request({
            url: fileUrl,
            method: 'HEAD'
            }, function(error, response, body){
            setResponseHeaders(response.headers);
            pipeToResponse();
            });

              function setResponseHeaders(headers){
                  positions = range.replace(/bytes=/, "").split("-");
                  start = parseInt(positions[0], 10); 
                  total = headers['content-length'];
                  end = positions[1] ? parseInt(positions[1], 10) : total - 1;
                  chunksize = (end-start)+1;
                
                  res.writeHead(206, { 
                    "Content-Range": "bytes " + start + "-" + end + "/" + total, 
                    "Accept-Ranges": "bytes",
                    "Content-Length": chunksize,
                    "Content-Type":"video/mp4"
                  });
                }
                
                function pipeToResponse() {
                  var options = {
                    url: fileUrl,
                    headers: {
                      range: "bytes=" + start + "-" + end,
                      connection: 'keep-alive'
                    }
                  };
                
                  request(options).pipe(res);
                }


    }
    catch(e){
      response['status'] = false
      response['message'] = "e.message"
      return res.status(400).json(response)
    }
  }



  